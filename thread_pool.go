// Copyright (c) 2022 Yaohui Wang (yaohuiwang@outlook.com)
// utils is licensed under Mulan PubL v2.
// You can use this software according to the terms and conditions of the Mulan PubL v2.
// You may obtain a copy of Mulan PubL v2 at:
//         http://license.coscl.org.cn/MulanPubL-2.0
// THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
// EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
// MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
// See the Mulan PubL v2 for more details.
package utils

import (
	"fmt"
	"sync"
)

type ThreadPool struct {
	QueueSize int
	PoolSize  int
	Queue     chan Runnable
	wg        *sync.WaitGroup
	destroy   bool
}

type Runnable interface {
	Run()
}

const (
	DefaultThreadQueueSize int = 64
	DefaultThreadPoolSize  int = 8
)

func DefaultThreadPool() *ThreadPool {
	return NewThreadPool(DefaultThreadQueueSize, DefaultThreadPoolSize)
}

func NewThreadPool(QueueSize int, PoolSize int) *ThreadPool {
	pool := &ThreadPool{
		QueueSize: QueueSize,
		PoolSize:  PoolSize,
		Queue:     make(chan Runnable, QueueSize),
		wg:        &sync.WaitGroup{},
		destroy:   false,
	}
	pool.Init()
	return pool
}

func (p *ThreadPool) newThread() {
	go func() {
		for !p.destroy {
			r := <-p.Queue
			r.Run()
			p.wg.Done()
		}
	}()
}

func (p *ThreadPool) Init() {
	for i := 0; i < p.QueueSize; i++ {
		p.newThread()
	}
}

func (p *ThreadPool) Wait() {
	p.wg.Wait()
}

func (p *ThreadPool) Destroy() {
	p.destroy = true
	close(p.Queue)
}

func (p *ThreadPool) Put(r Runnable) error {
	select {
	case p.Queue <- r:
		p.wg.Add(1)
		return nil
	default:
		return fmt.Errorf("thread queue is full")
	}
}
